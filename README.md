# sails-simplest

a [Sails](http://sailsjs.org) application

The purpose of this repo is to offer a simple proof of concept for a Sails Solr adapter.

###Setup
 - Install a Solr instance and configure it using the schema.xml and solrconfig.xml expected by [solr-client](https://github.com/lbdremy/solr-node-client).
 - Add a core named *test* —or—
 - Add a core with any name and override the core name set in connections.js in your local configuration for Sails (local.js).
 - Lift your Sails.

You should be able to POST a document with any of the attributes in Note.js and see it saved in Solr.
