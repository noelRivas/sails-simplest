/**
 * NoteController
 *
 * @description :: Server-side logic for managing notes
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	create: function(req, res) {
		var note = req.params.all();

		Note.create(note).exec(function createCB(err, created) {
			if(err) {
				res.status(err.status);
				res.json(err);
			}
			else {
				res.json(created);
			}
		})
	},
};