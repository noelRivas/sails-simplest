/**
* Note.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

// module.exports = {
// 	schema: false,
//   attributes: {

//   },
//   connection: 'solr',
//   autoPK: false
// };

module.exports = {
  schema: true,
  connection: 'solr',
  autoPK: false,
  attributes: {
    id: {
      type: 'string',
      primaryKey: true,
      //required: true
    },
    name_s: {
      type: 'string',
      //required: true,
    },
    body_t: {
      type: 'text',
      //required: true,
    },
    pinCode_i: { type: 'integer' },
    followUp_t: { type: 'string' },

    topics_ss: { type: 'string'},

    attendees_ss: { type: 'string'},

    // 0000: 0 Only me
    // 0001: 1 Everyone
    // 0010: 2 Select users and groups 
    // 0100: 4 <free> 
    permissions_i: { type: 'integer' },

    usersWhiteList_ss: { type: 'string'},
    groupsWhiteList_ss: { type: 'string'},
    user_i: {
      type: 'integer',
      //required: true
    },
    timestamp_dt: {
      type: 'datetime',
      //required: true
    },
    type_s: { type: 'string' }

    //user: DS.belongsTo('user', {async: true} }, 
  }

};